import { AppRegistry } from 'react-native';
import modules from './src/modules';
import App from './src/App';

AppRegistry.registerComponent('AwesomeReactApp', () => App);
