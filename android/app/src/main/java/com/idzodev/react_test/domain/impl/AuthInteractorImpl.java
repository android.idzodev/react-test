package com.idzodev.react_test.domain.impl;

import com.facebook.react.bridge.ReactContext;
import com.idzodev.react_test.domain.AuthInteractor;
import com.idzodev.react_test.modules.AuthModule;

import io.reactivex.Completable;
import io.reactivex.CompletableEmitter;
import io.reactivex.CompletableOnSubscribe;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class AuthInteractorImpl implements AuthInteractor {

    @Override
    public Completable login(final ReactContext reactContext, final String login, final String password){
        return Completable.create(emitter -> reactContext
                .getNativeModule(AuthModule.class)
                .login(login, password, emitter))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
