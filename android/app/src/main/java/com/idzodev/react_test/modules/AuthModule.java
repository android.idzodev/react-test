package com.idzodev.react_test.modules;

import com.facebook.react.bridge.CatalystInstance;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableNativeArray;

import io.reactivex.CompletableEmitter;

public class AuthModule extends ReactContextBaseJavaModule {

    private CompletableEmitter emitter;

    public AuthModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "AuthInteractorCallback";
    }

    @ReactMethod
    public void loginResult(ReadableMap readableMap){
        if (emitter == null){
            return;
        }

        if (readableMap.hasKey("exception")){
            ReadableMap exception = readableMap.getMap("exception");
            emitter.onError(new Exception(exception.getString("message")));
            emitter = null;
            return;
        }

        emitter.onComplete();
        emitter = null;
    }

    public void login(String login, String password, CompletableEmitter completableObserver){
        this.emitter = completableObserver;
        CatalystInstance catalystInstance = getReactApplicationContext().getCatalystInstance();
        WritableNativeArray params = new WritableNativeArray();
        params.pushString(login);
        params.pushString(password);
        catalystInstance.callFunction("AuthInteractor", "login", params);
    }
}
