package com.idzodev.react_test;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.react.ReactActivity;
import com.facebook.react.ReactInstanceManager;
import com.facebook.react.bridge.CatalystInstance;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.WritableNativeArray;
import com.idzodev.react_test.R;
import com.idzodev.react_test.domain.AuthInteractor;
import com.idzodev.react_test.domain.impl.AuthInteractorImpl;
import com.idzodev.react_test.ui.ProfileActivity;

import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;


public class MainActivity extends ReactActivity implements ReactInstanceManager.ReactInstanceEventListener {

    private EditText vLogin;
    private EditText vPassword;
    private Button vLoginBtn;
    private View vContent;
    private View vProgress;

    private AuthInteractor authInteractor = new AuthInteractorImpl();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        vLogin = findViewById(R.id.login);
        vLoginBtn = findViewById(R.id.login_btn);
        vPassword = findViewById(R.id.password);
        vContent = findViewById(R.id.content);
        vProgress = findViewById(R.id.progress);
        showProgress(true);

        getReactInstanceManager().addReactInstanceEventListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getReactInstanceManager().removeReactInstanceEventListener(this);
    }

    @Override
    protected String getMainComponentName() {
        return "AwesomeReactApp";
    }

    @Override
    public void onReactContextInitialized(ReactContext context) {
        showProgress(false);


        vLoginBtn.setOnClickListener(v -> {
            showProgress(true);
            authInteractor.login(
                    context,
                    vLogin.getText().toString(),
                    vPassword.getText().toString()
            ).subscribe(this::startHome, throwable -> {
                showProgress(false);
                Toast.makeText(getApplicationContext(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
            });
        });
    }

    private void startHome(){
        startActivity(new Intent(this, ProfileActivity.class));
        finish();
    }

    private void showProgress(boolean show){
        vProgress.setVisibility(show ? View.VISIBLE : View.GONE);
        vContent.setVisibility(show ? View.GONE : View.VISIBLE);
    }
}
