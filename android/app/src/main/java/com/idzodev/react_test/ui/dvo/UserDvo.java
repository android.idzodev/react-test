package com.idzodev.react_test.ui.dvo;

public class UserDvo {
    private String id;
    private String name;
    private String surname;

    public UserDvo(String id, String name, String surname) {
        this.id = id;
        this.name = name;
        this.surname = surname;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }
}
