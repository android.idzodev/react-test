package com.idzodev.react_test.modules;

import android.util.Log;

import com.facebook.react.bridge.CatalystInstance;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableNativeArray;
import com.idzodev.react_test.ui.dvo.UserDvo;

import io.reactivex.SingleEmitter;

public class UserModule extends ReactContextBaseJavaModule {

    private SingleEmitter<UserDvo> emitter;

    public UserModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "UserInteractorCallback";
    }

    public void getUser(SingleEmitter<UserDvo> emitter) {
        this.emitter = emitter;
        CatalystInstance catalystInstance = getReactApplicationContext().getCatalystInstance();
        catalystInstance.callFunction("UserInteractor", "getUser", new WritableNativeArray());
    }

    @ReactMethod
    public void getUserResult(ReadableMap readableMap){
        if (emitter == null){
            return;
        }
        if (readableMap.hasKey("exception")){
            ReadableMap exception = readableMap.getMap("exception");
            emitter.onError(new Exception(exception.getString("message")));
            emitter = null;
            return;
        }
        UserDvo user = map(readableMap.getMap("user"));
        emitter.onSuccess(user);
        emitter = null;
    }

    private UserDvo map(ReadableMap user){
        return new UserDvo(
                user.getString("id"),
                user.getString("name"),
                user.getString("surname")
        );
    }
}
