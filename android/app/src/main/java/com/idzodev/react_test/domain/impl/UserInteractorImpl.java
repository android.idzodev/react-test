package com.idzodev.react_test.domain.impl;

import com.facebook.react.bridge.ReactContext;
import com.idzodev.react_test.domain.UserInteractor;
import com.idzodev.react_test.modules.UserModule;
import com.idzodev.react_test.ui.dvo.UserDvo;

import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class UserInteractorImpl implements UserInteractor {

    @Override
    public Single<UserDvo> getUser(final ReactContext reactContext) {
        return Single.create((SingleOnSubscribe<UserDvo>) emitter ->
                reactContext.getNativeModule(UserModule.class).getUser(emitter)).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
