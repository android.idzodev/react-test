package com.idzodev.react_test.domain;

import com.facebook.react.bridge.ReactContext;

import io.reactivex.Completable;

public interface AuthInteractor {


    Completable login(ReactContext reactContext, String login, String password);



}
