package com.idzodev.react_test.domain;

import com.facebook.react.bridge.ReactContext;
import com.idzodev.react_test.ui.dvo.UserDvo;

import io.reactivex.Single;

public interface UserInteractor {

    Single<UserDvo> getUser(ReactContext reactContext);
}
