package com.idzodev.react_test.ui;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.react.ReactActivity;
import com.facebook.react.ReactInstanceManager;
import com.facebook.react.bridge.ReactContext;
import com.idzodev.react_test.App;
import com.idzodev.react_test.R;
import com.idzodev.react_test.domain.UserInteractor;
import com.idzodev.react_test.domain.impl.UserInteractorImpl;

public class ProfileActivity extends ReactActivity implements ReactInstanceManager.ReactInstanceEventListener  {

    private View vContent;
    private View vProgress;
    private TextView vId;
    private TextView vName;
    private TextView vSurname;


    private UserInteractor userInteractor = new UserInteractorImpl();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_activity);
        vContent = findViewById(R.id.content);
        vProgress = findViewById(R.id.progress);
        vName = findViewById(R.id.name);
        vSurname = findViewById(R.id.surname);
        vId = findViewById(R.id.id);
        showProgress(true);

        ReactContext reactContext = ((App)getApplicationContext()).getReactNativeHost().getReactInstanceManager().getCurrentReactContext();
        if (reactContext != null){
            onReactContextInitialized(reactContext);
            return;
        }

        getReactInstanceManager().addReactInstanceEventListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getReactInstanceManager().removeReactInstanceEventListener(this);
    }

    @Override
    protected String getMainComponentName() {
        return "AwesomeReactApp";
    }

    @Override
    public void onReactContextInitialized(ReactContext context) {
        Log.e("rect","onReactContextInitialized");
        userInteractor.getUser(context)
                .subscribe(userDvo -> {
                    showProgress(false);
                    vId.setText(userDvo.getId());
                    vSurname.setText(userDvo.getSurname());
                    vName.setText(userDvo.getName());
                }, throwable -> {
                    showProgress(false);
                    Toast.makeText(getApplicationContext(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
                });
    }

    private void showProgress(boolean show){
        vProgress.setVisibility(show ? View.VISIBLE : View.GONE);
        vContent.setVisibility(show ? View.GONE : View.VISIBLE);
    }
}
