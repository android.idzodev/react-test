import { NativeModules } from 'react-native';

export default {
    getUser(){
        var promise = new Promise(function(resolve, reject) {
            setTimeout(() => {
                resolve({ user:{
                    id: "1",
                    name: "John",
                    surname: "Doe"
                }});
            }, 2000);
        });

        promise
            .then(
                result => {
                    NativeModules.UserInteractorCallback.getUserResult(result);
                },
                error => {
                    NativeModules.UserInteractorCallback.getUserResult({
                        exception: {
                            message: error.message
                        }
                    });
                }
            );
    }
}