import { NativeModules } from 'react-native';

export default {
    login(login, password){
        var promise = new Promise(function(resolve, reject) {
            setTimeout(() => {
                if (login === 'admin' && password === "admin"){
                    resolve({});
                } else {
                    reject({message:"Wrong credentials"})
                }
            }, 2000);
        });

        promise
            .then(
                result => {
                    console.log("js success");
                    NativeModules.AuthInteractorCallback.loginResult({});
                },
                error => {
                    console.log("js failed 222");
                    NativeModules.AuthInteractorCallback.loginResult({
                        exception: {
                            message: error.message
                        }
                    });
                }
            );
    }
}