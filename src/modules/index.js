//register js modules
import BatchedBridge from "BatchedBridge"
import AuthInteractor from "../external/AuthInteractor"
import UserInteractor from "../external/UserInteractor"

BatchedBridge.registerCallableModule('AuthInteractor', AuthInteractor);
BatchedBridge.registerCallableModule('UserInteractor', UserInteractor);

